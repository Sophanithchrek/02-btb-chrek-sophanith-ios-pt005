//
//  Article.swift
//  RESTfulAPI
//
//  Created by SOPHANITH CHREK on 8/12/20.
//

import Foundation
import SwiftyJSON

struct Article {
    var id: String
    var title: String
    var description: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
    }
}
