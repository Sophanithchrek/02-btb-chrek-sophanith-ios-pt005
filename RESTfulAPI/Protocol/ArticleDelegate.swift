//
//  ArticleDelegate.swift
//  RESTfulAPI
//
//  Created by SOPHANITH CHREK on 8/12/20.
//

import Foundation

protocol ArticleDelegate {
    func sendData(id: String)
}
