//
//  AddArticleViewController.swift
//  RESTfulAPI
//
//  Created by SOPHANITH CHREK on 8/12/20.
//

import UIKit
import Alamofire


class AddArticleViewController: UIViewController {

//    {
//      "title": "string",
//      "description": "string",
//      "published": true,
//      "image": "string"
//    }
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var txtArticleTitle: UITextField!
    @IBOutlet weak var textViewDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnSaveArticlePressed(_ sender: UIButton) {
        
        let title = txtArticleTitle.text!
        let description = textViewDescription.text!
        
        if title != "" && description != "" {
            let articleParam: Parameters = [
                "title": title,
                "description": description,
                "published": true,
                "image": "string"
            ]
            
            AF.request("http://110.74.194.124:3000/api/articles", method: .post, parameters: articleParam, encoding: JSONEncoding.default).responseJSON(completionHandler: { response in
                switch response.result {
                case .success( _):
                    // come back to ViewController
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "vcForm") as! ViewController
                    vc.modalPresentationStyle = .fullScreen
                    vc.fetchData()
                    self.present(vc, animated: true, completion: nil)
                    
                case .failure(let error):
                    print(error)
                }
            })
        }
        
    }
    
}
