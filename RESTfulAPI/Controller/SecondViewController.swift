//
//  SecondViewController.swift
//  RESTfulAPI
//
//  Created by SOPHANITH CHREK on 8/12/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class SecondViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtArticleTitle: UITextField!
    @IBOutlet weak var textViewDescription: UITextView!

    var articleId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func fetchOne(id: String) {
        // Get data from API in JSON format
        AF.request("http://110.74.194.124:3000/api/articles/\(id)").responseData { response in
            switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let articleTemp = Article(json: json["data"])
                    
                    self.txtArticleTitle.text = articleTemp.title
                    self.textViewDescription.text = articleTemp.description
                case .failure(_):
                    print("Fetch Failed")
            }
        }
    }
    
    @IBAction func btnUpdateArticlePressed(_ sender: Any) {
        let title = txtArticleTitle.text!
        let description = textViewDescription.text!
        
        if title != "" && description != "" {
            let articleParamForUpdate: Parameters = [
                "title": title,
                "description": description,
                "published": true,
                "image": "string"
            ]
            
            AF.request("http://110.74.194.124:3000/api/articles/\(articleId)", method: .patch, parameters: articleParamForUpdate, encoding: JSONEncoding.default).responseJSON(completionHandler: { response in
                switch response.result{
                case .success(let value):
                    print(value)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "vcForm") as! ViewController
                    vc.fetchData()
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                case .failure(let error):
                    print(error)
                }
            })
        } else {
            let alert = UIAlertController(title: "Please input any text!", message: nil,
            preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnDeletePressed(_ sender: Any) {
        let alert = UIAlertController(title: "Do you want to delete?", message: nil,
        preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
        }))
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: {(_: UIAlertAction!) in
            // Delete Data from API
            AF.request("http://110.74.194.124:3000/api/articles/\(self.articleId)", method: .delete).response { response in
                
                // come back to ViewController
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "vcForm") as! ViewController
                vc.modalPresentationStyle = .fullScreen
                vc.fetchData()
                self.present(vc, animated: true, completion: nil)
                
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SecondViewController: ArticleDelegate {
    func sendData(id: String) {
        fetchOne(id: id)
        self.articleId = id
    }
}
