//
//  ViewController.swift
//  RESTfulAPI
//
//  Created by SOPHANITH CHREK on 7/12/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    // Create empty array to store data from API
    var article: [Article] = []
    
    var articleDelegate: ArticleDelegate?
    
    @IBOutlet weak var articleTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        articleTableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        articleTableView.dataSource = self
        articleTableView.delegate = self
    }
    
    func fetchData() {
        // Get data from API in JSON format
        AF.request("http://110.74.194.124:3000/api/articles").responseData { response in
            switch response.result {
                case .success(let value):
                    var articleTemp = [Article]()
                    let json = JSON(value)
                    // Loop JSON data
                    for (_, subJson):(String, JSON) in json["data"] {
                        articleTemp.append(Article(json: subJson))
                    }
                    self.article = articleTemp
                    DispatchQueue.main.async {
                        self.articleTableView.reloadData()
                    }
                case .failure(_):
                    print("Fetch Failed")
            }
        }
    }
    
    @IBAction func btnAddItemPressed(_ sender: Any) {
        let addArticleVC = self.storyboard?.instantiateViewController(withIdentifier: "addArticleForm") as! AddArticleViewController
        addArticleVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(addArticleVC, animated: true)
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.article.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = articleTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell
        cell.lblId.text = article[indexPath.row].id
        cell.lblTitle.text = article[indexPath.row].title
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "secondForm") as! SecondViewController
        articleDelegate = secondVC
        // send data to secondViewController
        articleDelegate?.sendData(id: article[indexPath.row].id)
          
        secondVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return CGFloat(150)
//    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let deleteId = article[indexPath.row].id
            // Delete Data from API
            AF.request("http://110.74.194.124:3000/api/teachers/\(deleteId)", method: .delete).response { response in
                DispatchQueue.main.async {
                    self.articleTableView.reloadData()
                }
                self.article.remove(at: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        let open = UIAction(title: "Open", image: UIImage(systemName: "square.and.arrow.up")) { _ in
            let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "secondForm") as! SecondViewController
            self.articleDelegate = secondVC
            // send data to secondViewController
            self.articleDelegate?.sendData(id: self.article[indexPath.row].id)
              
            secondVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(secondVC, animated: true)
        }
        let delete = UIAction(title: "Delete", image: UIImage(systemName: "doc.on.doc")) { _ in
            print("Delete with id: \(self.article[indexPath.row].id)")
            let deleteId = self.article[indexPath.row].id
            // Delete Data from API
            AF.request("http://110.74.194.124:3000/api/teachers/\(deleteId)", method: .delete).response { response in
                DispatchQueue.main.async {
                    self.articleTableView.reloadData()
                }
                self.article.remove(at: indexPath.row)
            }
        }
        
        // Pass the indexPath as the identifier for the menu configuration
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            // Empty menu for demonstration purposes
            return UIMenu(title: "", children: [open, delete])
        }
    }
}
