//
//  ArticleTableViewCell.swift
//  RESTfulAPI
//
//  Created by SOPHANITH CHREK on 7/12/20.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
